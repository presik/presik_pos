import logging
import os
from datetime import date

if os.name == 'posix':
    homex = 'HOME'
    dirconfig = '.tryton'
    temp_log = '/tmp'
elif os.name == 'nt':
    homex = 'USERPROFILE'
    dirconfig = 'AppData/Local/tryton'
    temp_log = 'AppData/Local/Temp'

HOME_DIR = os.getenv(homex)
default_dir = os.path.join(HOME_DIR, dirconfig)
date_str = date.today().strftime('%Y-%m')
if os.path.exists(default_dir):
    log_file_path = default_dir + f'/presik_pos-{date_str}.log'
else:
    log_file_path = temp_log + f'/presik_pos-{date_str}.log'

log_formater = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(funcName)s - %(name)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S', style="%")


handler_console = logging.StreamHandler()
handler_console.setFormatter(log_formater)

handler_file = logging.FileHandler(log_file_path, mode='a', encoding='utf-8')
handler_file.setFormatter(log_formater)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(handler_console)
logger.addHandler(handler_file)
