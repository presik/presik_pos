import locale
import logging
import os
from pathlib import Path

from PySide6.QtCore import QRegularExpression, Qt
from PySide6.QtGui import QDoubleValidator, QRegularExpressionValidator
from PySide6.QtWidgets import (
    QCalendarWidget,
    QCheckBox,
    QComboBox,
    QCompleter,
    QGridLayout,
    QLabel,
    QLineEdit,
    QPlainTextEdit,
    QSpinBox,
    QTextEdit,
    QTreeView,
)

from .qt_models import get_simple_model

regex_ = QRegularExpression("^\\d{1,3}(([.]\\d{3})*),(\\d{2})$")
validator = QRegularExpressionValidator(regex_)
pkg_dir = str(Path(os.path.dirname(__file__)).parents[0])
logger = logging.getLogger(__name__)

try:
    locale.setlocale(locale.LC_ALL, 'es_CO.UTF-8')
except Exception:
    logger.exception('error locale')
    print("Warning: Error setting locale")

__all__ = ['CheckBox', 'ComboBox', 'Field', 'FieldDate', 'FieldInteger', 'FieldMoney', 'GridForm', 'Label']


def set_object_name(obj, type_, value):
    size = 'small'
    color = 'gray'
    if value.get('size'):
        size = value.get('size')
    if value.get('color'):
        color = value.get('color')

    name = type_ + size + '_' + color
    if value.get('font_size'):
        font_size = value.get('font_size')
        name += '_' + font_size
    obj.setObjectName(name)


class Completer(QCompleter):

    def __init__(self, parent, records, fields):
        super(Completer, self).__init__()

        self.parent = parent
        self.treeview_search = QTreeView()
        col_headers = self.treeview_search.header()
        col_headers.hide()
        self.setPopup(self.treeview_search)
        self.fields = fields
        self._set_model(records, fields)
        self.activated.connect(self.on_accept)
        self.setFilterMode(Qt.MatchContains)
        self.setCaseSensitivity(Qt.CaseInsensitive)
        self.setWrapAround(True)
        self.setCompletionColumn(1)

        self.treeview_search.setColumnWidth(1, 300)
        self.treeview_search.setColumnHidden(0, True)
        self.id = None

    def get_values(self, records):
        vkeys = [f[0] for f in self.fields]
        values = []
        values_extend = values.extend
        for r in records:
            row = []
            row_extend = row.extend
            for key in vkeys:
                if '.' in key:
                    attrs = key.split('.')
                    val = r.copy()
                    name = attrs[-1]
                    for a in attrs[:-1]:
                        val = val[a + '.']
                    val = val[name]
                else:
                    val = r[key]
                row_extend([val])
            values_extend([row])
        return values

    def _set_model(self, records, headers):
        headers = [f[1] for f in self.fields]
        values = self.get_values(records)
        self.model = get_simple_model(self.parent, values, headers)
        self.setModel(self.model)

    def on_accept(self):
        model_index = self._get_model_index()
        idx = self.model.index(model_index.row(), 0)
        self.id = idx.data()

    def _get_model_index(self):
        item_view = self.popup()
        index = item_view.currentIndex()
        proxy_model = self.completionModel()
        model_index = proxy_model.mapToSource(index)
        return model_index


class Label(QLabel):

    def __init__(self, obj, key, value, align='right', form=None):
        super(Label, self).__init__()
        self.key = key
        self.setText(value['name'] + ':')
        set_object_name(self, 'label_', value)
        if align == 'left':
            self.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        else:
            self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)


class Field(QLineEdit):

    def __init__(self, obj, key, value, type=None, form=None):
        super(Field, self).__init__()
        setattr(obj, 'field_' + key, self)
        self.key = key
        self.form = form
        self.parent = obj
        set_object_name(self, 'field_', value)
        if value.get('type') == 'numeric':
            self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        elif value.get('type') == 'relation':
            self.set_completer(
                value.get('model'), value.get('fields'), value.get('domain'),
            )

    def set_completer(self, model, fields, domain=[]):
        records = model.find(domain)
        self.completer = Completer(self.parent, records, fields)
        self.setCompleter(self.completer)

    def get_id(self):
        return self.completer.id


class TextField(QTextEdit):

    def __init__(self, obj, key, value, form=None):
        super(TextField, self).__init__()
        self.key = key
        self.form = form
        setattr(obj, 'field_' + key, self)
        set_object_name(self, 'field_', value)
        self.value_changed = False

    def textChanged(self, text):
        self.value_changed = True


class TextAreaField(QPlainTextEdit):

    def __init__(self, obj, key, value, form=None):
        super(TextAreaField, self).__init__()
        self.key = key
        self.form = form
        setattr(obj, 'field_' + key, self)
        set_object_name(self, 'field_', value)
        self.value_changed = False

    def textChanged(self, text):
        self.value_changed = True


class CheckBox(QCheckBox):

    def __init__(self, obj, key, value, form=None):
        super(CheckBox, self).__init__()
        self.key = key
        self.form = form
        setattr(obj, 'field_' + key, self)
        self.setText(value.get('name'))


class FieldDate(QCalendarWidget):

    def __init__(self, obj, key, value, form=None):
        super(FieldDate, self).__init__()
        self.key = key
        self.form = form
        setattr(obj, 'field_' + key, self)
        set_object_name(self, 'field_', value)
        self.value_changed = False
        self.key = key
        self.selectionChanged.connect(self.on_change)
        if value.get('default'):
            self.form.setState(self.key, value.get('default'))

    def get_value(self):
        return self.selectedDate().toPython()

    def on_change(self):
        date_ = self.selectedDate().toPython()
        if self.form:
            self.form.setState(self.key, date_)


class FieldMoney(QLineEdit):

    def __init__(self, obj, key, value={}, amount=None, digits=2, readonly=True, form=None):
        super(FieldMoney, self).__init__()
        setattr(obj, 'field_' + key, self)
        self.key = key
        self.form = form
        set_object_name(self, 'field_', value)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.digits = 0
        self.value_changed = False
        self.textEdited.connect(self.value_edited)
        self._text = '0'
        self.amount = 0
        self.setReadOnly(readonly)
        validator = QDoubleValidator()
        validator.setDecimals(2)
        self.setValidator(validator)
        if not amount:
            self.zero()

    # def __str__(self):
    #     return self.format_text()

    def format_text(self, text_):
        amount = float(text_)
        return format(round(amount), ',d')

    def setText(self, amount):
        if not amount:
            text = ''
        else:
            text = self.format_text(amount)
        super(FieldMoney, self).setText(str(text))

    def zero(self):
        self.setText(str(0))

    def value_edited(self, amount):
        self.value_changed = True

    def show(self):
        pass

    def get_value(self):
        return self.text()


class FieldInteger(QSpinBox):

    def __init__(self, obj, key, data, maximun=None, readonly=False, form=None):
        super(FieldInteger, self).__init__()
        setattr(obj, 'field_' + key, self)
        self.key = key
        self.form = form
        self.parent = obj
        set_object_name(self, 'field_', data)
        self.setAlignment(Qt.AlignCenter)
        self.valueChanged.connect(self.value_edited)
        self.default = data.get('default')
        if data.get('default'):
            self.setText(data.get('default'))
        self.setReadOnly(readonly)
        self.setMinimum(data.get('minimun', 0))
        if data.get('maximun'):
            self.setMaximum(data.get('maximun'))

        self.setStyleSheet(f"""
            QSpinBox {{
                border: 1px solid #ABABAB;
                border-radius: 3px;
                text-center;
                }}

            QSpinBox::up-button  {{
                subcontrol-origin: margin;
                subcontrol-position: center right;
                image: url({pkg_dir + '/share/rightArrow.svg'});
                background-color: #BCC8D0;
                border-radius: 0px 10px 10px 0px;
                right: 1px;
            }}

            QSpinBox::down-button  {{
                subcontrol-origin: margin;
                subcontrol-position: center left;
                image: url({pkg_dir + '/share/leftArrow.svg'});
                background-color: #BCC8D0;
                border-radius: 10px 0px 0px 10px;
                left: 1px;
            }}
        """)

    def value_edited(self):
        try:
            if self.parent.store.store.get('persons') != self.value():
                self.parent.store.update({'persons': self.value()})
        except Exception:
            pass

    def setText(self, value):
        if value in (None, ''):
            if self.default:
                self.setValue(int(self.default))
        else:
            self.setValue(int(value))


class FieldInput(QLineEdit):

    def __init__(self, obj, key, value, amount=None, digits=2, form=None):
        super(FieldInput, self).__init__()
        setattr(obj, 'field_' + key, self)
        self.key = key
        self.form = form
        self.parent = obj
        set_object_name(self, 'field_', value)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.digits = 0
        self.field = key
        self.value_changed = False
        self.textEdited.connect(self.value_edited)
        self.editingFinished.connect(self.finished_edit)
        self._text = '0'
        self.amount = 0
        validator = QDoubleValidator()
        validator.setDecimals(2)
        self.setValidator(validator)
        self.method_on_change = obj.on_change_input
        if not amount:
            self.zero()

    def __str__(self):
        return self.format_text()

    def format_text(self, text_):
        amount = float(text_)
        return format(round(amount), ',d')

    def setText(self, amount):
        text = "" if not amount else self.format_text(amount)
        super(FieldInput, self).setText(str(text))

    def zero(self):
        self.setText(str(0))

    def value_edited(self, amount):
        self.value_changed = True

    def finished_edit(self):
        amount = self.text()
        self.method_on_change(self.field, amount)

    def show(self):
        pass


class FieldNumeric(QLineEdit):

    def __init__(self, obj, key, value, amount=None, digits=2, readonly=True, form=None):
        super(FieldNumeric, self).__init__()
        setattr(obj, 'field_' + key, self)
        set_object_name(self, 'field_', value)
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.key = key
        self.form = form
        self.digits = 2
        self.value_changed = False
        self.textEdited.connect(self.value_edited)
        self.textChanged.connect(self.text_changed)
        self._text = '0'
        self.amount = 0
        self.parent = obj
        self.setReadOnly(readonly)
        validator = QDoubleValidator()
        validator.setDecimals(2)
        self.setValidator(validator)
        if not amount:
            self.zero()

    def __str__(self):
        return self.format_text()

    def setText(self, amount):
        text = amount if amount else ""
        text = str(text).lstrip('0')
        super(FieldNumeric, self).setText(text)

    def zero(self):
        self.setText(str(0))

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Backspace:
            self.parent.key_backspace_pressed()
        super(FieldNumeric, self).keyPressEvent(event)

    def value_edited(self, amount):
        self.value_changed = True

    def text_changed(self, amount):
        if amount == '':
            return
        amount = amount.replace(',', '')
        text = f"{float(amount):,.2f}"
        self.setText(text)
        self.setCursorPosition(len(text) - 3)

    def show(self):
        pass

    def get_value(self):
        amount = self.text()
        amount = amount.replace(',', '')
        return float(amount)


class ComboBox(QComboBox):

    def __init__(self, obj, key, data, form=None):
        super(ComboBox, self).__init__()
        setattr(obj, 'field_' + key, self)
        self.parent = obj
        self.key = key
        self.form = form
        self.setFrame(True)
        self.setObjectName('field_' + key)
        self.setFocusPolicy(Qt.StrongFocus)
        # values = []
        # if data.get('values'):
        values = data.get('values', [])
        # heads = []
        # if data.get('heads', []):
        heads = data.get('heads', [])
        selection_model = get_simple_model(obj, values, heads)
        self.setModel(selection_model)
        self.setModelColumn(1)
        # selection_model.findItems(str(3), column=0)
        self.method_on_change = None
        self.currentIndexChanged.connect(self.on_change)
        if data.get('default'):
            self.set_from_id(data['default']['id'])
            if self.form:
                self.form.setState(self.key, self.get_id())
        if data.get('on_change'):
            self.method_on_change = getattr(self.parent, data.get('on_change'))

    def on_change(self, index):
        if self.method_on_change:
            self.method_on_change(index)
        if self.form:
            self.form.setState(self.key, self.get_id())

    def set_editable(self, value=True):
        self.setEditable(value)

    def set_enabled(self, value=True):
        self.setEnabled(value)

    def get_value(self):
        return self.get_id()

    def wheelEvent(self, event):
        if self.hasFocus():
            super(ComboBox, self).wheelEvent(event)

    def get_id(self):
        model = self.model()
        row = self.currentIndex()
        if row >= 0:
            column = 0  # id ever is column Zero
            res = model.item(row, column)
            return res.text()
        else:
            return None

    def hidePopup(self):
        try:
            super(ComboBox, self).hidePopup()
        except Exception:
            logger.exception('error hide popup')

    def showPopup(self):
        super(ComboBox, self).showPopup()

    def get_label(self):
        model = self.model()
        row = self.currentIndex()
        column = 1  # id ever is column Zero
        res = model.item(row, column)
        return res.text()

    def set_from_id(self, id_):
        model = self.model()
        items = model.findItems(str(id_), column=0)
        idx = model.indexFromItem(items[0])
        self.setCurrentIndex(idx.row())

    def updateComboBox(self, id_):
        "only call this function if you don't need require execute onchange"
        # Block signals temporarily
        self.blockSignals(True)

        # Set the desired value in the QComboBox
        if id_:
            model = self.model()
            items = model.findItems(str(id_), column=0)
            idx = model.indexFromItem(items[0])
            self.setCurrentIndex(idx.row())
        else:
            self.setCurrentIndex(-1)

        # Unblock signals after setting the value
        self.blockSignals(False)

    def set_none(self):
        self.setCurrentIndex(0)


class GridForm(QGridLayout):
    """
    Add a simple form Grid Style to screen,
    from a data dict with set of {values, attributes}
    example:
        (field_name, {
            'name': string descriptor,
            'readonly': Bool,
            'type': type_widget,
            'placeholder': True or False,
            'values': ['a', 'b',...] - optional
        }),
    col:: is number of columns
    type_widget :: field or selection
    """

    def __init__(self, obj, values, col=1, size=None):
        super(GridForm, self).__init__()
        self.setSpacing(5)
        if size != 'large':
            self.setSpacing(3)

        row = 1
        cols = 0
        align = 'right'
        self.store = {}
        if col == 0:
            align = 'left'

        TYPES_ = {
            'selection': ComboBox,
            'checkbox': CheckBox,
            'money': FieldMoney,
            'text': TextField,
            'text_area': TextAreaField,
            'date': FieldDate,
            'editable': FieldInput,
            'integer': FieldInteger,
        }

        setted_focus = False
        addWidget = self.addWidget
        for key, value in values.items():
            type_ = value.get('type')
            expand = 0
            self.store[key] = None
            try:
                Field_ = TYPES_[type_]
                _field = Field_(obj, key, value, form=self)

                if type_ in ('checkbox', 'text', 'text_area'):
                    expand = 1
                    if type_ == 'text':
                        align = 'left'
            except Exception:
                if key == 'persons':
                    logger.exception('error field')
                _field = Field(obj, key, value, form=self)
                if value.get('password') is True:
                    _field.setEchoMode(QLineEdit.Password)
                if value.get('placeholder'):
                    _field.setPlaceholderText(value['name'])

            if not setted_focus:
                self.focus_widget = _field
                setted_focus = True

            if not value.get('placeholder'):
                _label = Label(obj, key, value, align, form=self)
                setattr(self, 'label_' + key, _label)

            self.setRowStretch(row, 0)
            column1 = cols * col + 1 + expand
            column2 = column1 + 1 + expand
            if value.get('invisible') is True:
                continue
            if not expand:
                if not value.get('placeholder'):
                    addWidget(_label, row, column1)
                if col == 0:
                    row = row + 1
                    addWidget(_field, row, column1)
                else:
                    addWidget(_field, row, column2)
            else:
                addWidget(_label, row, column1, row, column2)
                addWidget(_field, row + 1, column1, row + 1, column2)

            if value.get('translate') is True:
                _field.translate = True
            if value.get('readonly') is True:
                _field.setReadOnly(True)
                _field.setFocusPolicy(Qt.NoFocus)

            if cols < (col - 1):
                cols += 1
            else:
                row += 1
                cols = 0

    def setState(self, key, value):
        self.store[key] = value

    def getStore(self):
        return self.store

    def set_focus(self):
        if hasattr(self, 'focus_widget'):
            self.focus_widget.setFocus()
