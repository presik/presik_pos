
from PySide6.QtCore import QByteArray, Qt, QUrl
from PySide6.QtGui import QGuiApplication, QPixmap
from PySide6.QtNetwork import QNetworkAccessManager, QNetworkRequest
from PySide6.QtWidgets import (
    QDialog,
    QHBoxLayout,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from ..constants import alignCenter
from ..tools import get_screen

__all__ = ['Image']


class Image(QLabel):

    def __init__(self, parent=None, name=None, default_img=None, scaled_rate=None):
        if not parent:
            parent = QWidget()
        super(Image, self).__init__(parent, Qt.Window)

        # screen = QDesktopWidget().screenGeometry()
        screen = QGuiApplication.primaryScreen().size()
        screen_width = screen.width()

        self.parent = parent
        self.setObjectName('img_' + name)

        if default_img:
            self.pixmap = QPixmap()
            self.pixmap.load(default_img)
            img_width, img_height = self.pixmap.width(), self.pixmap.height()
            scaled_rate = False
            if screen_width <= 1024:
                scaled_rate = 0.5
            elif screen_width <= 1366:
                scaled_rate = 0.75
            if scaled_rate:
                new_width = img_width * scaled_rate
                new_height = img_height * scaled_rate
                self.pixmap = self.pixmap.scaled(int(new_width), int(new_height),
                    Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.setPixmap(self.pixmap)

    def set_image(self, img, kind=None):
        self.pixmap = QPixmap()
        if img:
            if kind == 'bytes':
                ba = QByteArray.fromBase64(img)
                self.pixmap.loadFromData(ba)
            if kind == 'qimage':
                self.pixmap.convertFromImage(img)
            else:
                self.pixmap.loadFromData(img.data)
            self.setPixmap(self.pixmap)

    def load_image(self, pathfile):
        self.pixmap = QPixmap()
        self.pixmap.load(pathfile)
        self.setPixmap(self.pixmap)

    def activate(self):
        self.free_center()
        qlabel = QLabel()
        qlabel.setPixmap(self.pixmap)
        qhVox = QHBoxLayout()
        qhVox.addWidget(qlabel)
        self.parent.setLayout(qhVox)
        self.parent.show()

    def free_center(self):
        screen = QGuiApplication.primaryScreen().size()
        # screen = QDesktopWidget().screenGeometry()
        screen_width = screen.width()
        screen_height = screen.height()
        size = self.pixmap.size()
        self.parent.setGeometry(
            int((screen_width / 2) - (size.width() / 2)),
            int((screen_height / 2) - (size.height() / 2)),
            size.width(),
            size.height(),
        )


class ImageViewer(QDialog):
    def __init__(self, image_urls=[], parent=None):
        self.width, self.height = get_screen()
        super(ImageViewer, self).__init__(parent)
        self.setFixedSize(int(self.width * 0.7), int(self.height * 0.7))

        self.current_index = 0
        self.image_urls = image_urls

        self.image_label = QLabel(self)
        self.image_label.setAlignment(alignCenter)

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.image_label)

        self.previous_button = QPushButton('Anterior', self)
        self.next_button = QPushButton('Siguiente', self)

        button_layout = self.create_button_layout()
        self.layout.addLayout(button_layout)

        self.previous_button.clicked.connect(self.show_previous_image)
        self.next_button.clicked.connect(self.show_next_image)
        if self.image_urls:
            self.load_image_from_url(self.current_index)

    def create_button_layout(self):
        button_layout = QHBoxLayout()
        button_layout.addStretch()
        button_layout.addWidget(self.previous_button)
        button_layout.addWidget(self.next_button)
        button_layout.addStretch()
        return button_layout

    def load_image_from_url(self, index):
        if 0 <= index < len(self.image_urls):
            image_url = self.image_urls[index]
            network_manager = QNetworkAccessManager(self)
            request = QNetworkRequest(QUrl(image_url))
            network_manager.get(request).finished.connect(self.handle_image_download)

    def handle_image_download(self):
        reply = self.sender()
        data = reply.readAll()
        pixmap = QPixmap()
        pixmap.loadFromData(data)
        img_width, img_height = pixmap.width(), pixmap.height()
        scaled_rate = False
        if self.width <= 1024:
            scaled_rate = 1.5
        elif self.width <= 1366:
            scaled_rate = 1.75
        elif self.width > 1366:
            scaled_rate = 2.0
        if scaled_rate:
            new_width = img_width * scaled_rate
            new_height = img_height * scaled_rate
            pixmap = pixmap.scaled(int(new_width), int(new_height),
                Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.image_label.setPixmap(pixmap)
        # self.image_label.setScaledContents(True)
        self.setWindowTitle(f'Visor de Imágenes - {self.current_index + 1}')

    def show_previous_image(self):
        self.current_index = (self.current_index - 1) % len(self.image_urls)
        self.load_image_from_url(self.current_index)

    def show_next_image(self):
        self.current_index = (self.current_index + 1) % len(self.image_urls)
        self.load_image_from_url(self.current_index)
