# from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from PySide6.QtCore import Qt, QAbstractTableModel, QModelIndex
import math
from itertools import islice
_horizontal = Qt.Horizontal


__all__ = ['TableModel', 'TableEdit']


class TableEdit(QAbstractTableModel):
    def __init__(self, parent, data, fields):
        super(TableEdit, self).__init__()
        self._data = data
        self._parent = parent
        self._fields = fields

    def data(self, index, role):
        if role == Qt.DisplayRole:
            row = index.row()
            col = index.column()
            val = self._data[row][col]
            if self._fields[col]['type'] == 'integer':
                return int(val)
            elif self._fields[col]['type'] == 'float':
                if isinstance(val, (int, float)):
                    return "{:,.2f}".format(float(val))
                else:
                    return "{:,.2f}".format(float(val.replace(',', '')))
            return val
        elif role == Qt.TextAlignmentRole:
            col = index.column()
            if self._fields[col]['type'] in ['integer', 'float']:
                return Qt.AlignRight

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._fields) if self._fields else 0

    def clearData(self):
        self.beginRemoveRows(QModelIndex(), 0, len(self._data))
        self._data = []
        self.endRemoveRows()
        self.layoutChanged.emit()

    def get_data(self, index):
        raw_value = self._data[index.row()]
        return raw_value

    def setData(self, index, value, role):
        if role == Qt.EditRole:
            if not value:
                return False
            row = index.row()
            col = index.column()
            if self._fields[col]['type'] == 'integer':
                value = int(value)
            elif self._fields[col]['type'] == 'float':
                value = "{:,.2f}".format(float(value))
            self._data[row][col] = value
            on_change = self._fields[col].get('change')
            if on_change:
                method = getattr(self._parent, on_change)
                method(row)
            return True

    def flags(self, index):
        field = self._fields[index.column()]
        flags = Qt.ItemIsEnabled
        if not field.get('readonly'):
            flags |= Qt.ItemIsSelectable | Qt.ItemIsEditable
        return flags

    def headerData(self, section, orientation, role):
        """ Set the headers to be displayed. """
        if role != Qt.DisplayRole:
            return None
        try:
            return next(islice(self._fields, section, None))['label']
        except Exception as e:
            print(str(e))
            return None

    def add_record(self, rec):
        length = len(self._data)
        self.beginInsertRows(QModelIndex(), length, length)
        self._data.append(rec)
        self.endInsertRows()
        return rec

    def get_sum(self, col):
        return sum(float(d[col].replace(',', '') if isinstance(d[col], str) else d[col]) for d in self._data)


class TableModel(QAbstractTableModel):

    def __init__(self, model, fields):
        super(TableModel, self).__init__()
        self._fields = fields
        self.model = model
        self._data = []

    def reset(self):
        self.beginResetModel()
        self._data = []
        self.endResetModel()

    def add_record(self, rec):
        length = len(self._data)
        self.beginInsertRows(QModelIndex(), length, length)
        self._data.append(rec)
        self.endInsertRows()
        return rec

    def get_id(self):
        pass

    def removeId(self, row, mdl_idx):
        self.beginRemoveRows(mdl_idx, row, row)
        id_ = self._data[row].get('id')
        self._data.pop(row)
        self.endRemoveRows()
        return id_

    def deleteRecords(self, ids):
        pass

    def rowCount(self, parent=None):
        return len(self._data)

    def columnCount(self, parent=None):
        return len(self._fields) if self._fields else 0

    def get_data(self, index):
        try:
            raw_value = self._data[index.row()]
        except Exception as e:
            print(str(e))
            raw_value = {}
        return raw_value

    def get_value(self, data, field):
        value = ''
        list_ = field.split('.')
        if len(list_) > 1:
            for ln in list_[:-1]:
                ln = ln + '.'
                data = data[ln]
        try:
            value = data[list_[-1]]
        except Exception as e:
            print(data, field, str(e))
        return value

    def data(self, index, role, field_name='name'):
        field = self._fields[index.column()]
        if role == Qt.DisplayRole:
            index_row = self._data[index.row()]
            raw_value = ''
            if not index_row.get(field.get(field_name)):
                if '.' in field.get(field_name):
                    raw_value = self.get_value(index_row, field[field_name])
            else:
                raw_value = index_row[field[field_name]]

            digits = None
            if field.get('digits'):
                digits = 0
                target_field = field.get('digits')[0]
                target = index_row['unit.']['symbol']
                if target:
                    group_digits = field.get('digits')[1]
                    if group_digits.get(target):
                        digits = group_digits.get(target)
                        eval_value = math.floor(float(raw_value)) - float(raw_value)
                        if target == 'u' and (eval_value > 0 or eval_value < 0):
                            digits = 2
            if raw_value is None:
                raw_value = ''
            fmt_value = raw_value
            if raw_value and field.get('format'):
                field_format = field['format']
                if digits or digits == 0:
                    field_format = field['format'] % str(digits)
                if isinstance(raw_value, str) and raw_value != '':
                    raw_value = float(raw_value)
                if field_format == '{:,d}':
                    raw_value = int(raw_value)
                if raw_value:
                    fmt_value = field_format.format(raw_value)
            return fmt_value

        elif role == Qt.TextAlignmentRole:
            align = Qt.AlignmentFlag(Qt.AlignVCenter | field['align'])
            return align
        else:
            return None

    def get_sum(self, field_target):
        res = sum(d[field_target] for d in self._data)
        return res

    def update_record(self, rec, pos=None):
        if pos is None:
            pos = 0
            for d in self._data:
                if d['id'] == rec['id']:
                    break
                pos += 1
        self._data.pop(pos)
        self._data.insert(pos, rec)
        start_pos = self.index(pos, 0)
        end_pos = self.index(pos, len(self._fields) - 1)
        self.dataChanged.emit(start_pos, end_pos)
        return rec

    def headerData(self, section, orientation=_horizontal, role=None):
        """ Set the headers to be displayed. """
        if role != Qt.DisplayRole:
            return None
        elements = (f['description'] for f in self._fields)
        return next(islice(elements, section, None))
        # if orientation == Qt.Horizontal:
        # return None
