import os
from datetime import date
from pathlib import Path

from PySide6.QtCore import QRect, QSize, Qt
from PySide6.QtGui import QFont, QIcon, QPixmap
from PySide6.QtWidgets import (
    QGridLayout,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLayout,
    QLineEdit,
    QMenuBar,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from ..tools import get_screen
from ..version import __version__

pkg_dir = str(Path(os.path.dirname(__file__)).parents[0])
path_pos_advanced = os.path.join(pkg_dir, 'share', 'advanced_pos.png')

dimensions = {
    'large': (1173, 666),
    'medium': (980, 666),
    'small': (900, 666),
}

STYLE = """
        QMainWindow {
            margin: 0;
            background-color: rgb(246, 245, 244);
        }

"""


class Ui_Login:
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName("MainWindow")
        width, height = get_screen()
        screen_size = 'large'
        if width <= 1024:
            screen_size = 'small'
        elif width <= 1366:
            screen_size = 'medium'
        # MainWindow.resize(width*0.6109, height*0.6166)
        # MainWindow.resize(*dimensions[screen_size])
        width, height = dimensions[screen_size]
        MainWindow.resize(width, height)
        icon = QIcon()
        icon.addFile("../.pos_app/presik_pos_v2/app/share/pos-icon.ico", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setWindowOpacity(1.0)
        MainWindow.setStyleSheet(STYLE)
        MainWindow.setWindowTitle("Login presik pos")
        # MainWindow.setStyleSheet(u"margin:0;background-color: rgb(246, 245, 244);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridGroupBoxMain = QGroupBox(self.centralwidget)
        self.gridGroupBoxMain.setObjectName("gridGroupBoxMain")
        qrect = QRect(0, -10, width, height)
        # if screen_size == 'small':
        #     qrect = QRect(0, -10, width, height)
        self.gridGroupBoxMain.setGeometry(qrect)
        self.gridLayout_2 = QGridLayout(self.gridGroupBoxMain)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.setHorizontalSpacing(0)
        self.gridLayout.setVerticalSpacing(10)
        self.gridLayout.setContentsMargins(50, 50, 50, 20)
        self.vLayoutLogin = QVBoxLayout()
        self.vLayoutLogin.setSpacing(10)
        self.vLayoutLogin.setObjectName("vLayoutLogin")
        self.vLayoutLogin.setSizeConstraint(QLayout.SetMinAndMaxSize)
        self.vLayoutLogin.setContentsMargins(-1, 20, -1, 30)
        self.label_database = QLabel(self.gridGroupBoxMain)
        self.label_database.setObjectName("label_database")
        self.label_database.setText("Empresa")
        self.label_database.setMinimumSize(QSize(0, 0))
        font = QFont()
        font.setFamilies(["Sans Serif"])
        font.setPointSize(11)
        font.setBold(False)
        self.label_database.setFont(font)
        self.label_database.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_database.setTextFormat(Qt.PlainText)
        self.label_database.setAlignment(Qt.AlignBottom | Qt.AlignLeading | Qt.AlignLeft)

        self.vLayoutLogin.addWidget(self.label_database)

        self.field_database = QLineEdit(self.gridGroupBoxMain)
        self.field_database.setObjectName("field_database")
        self.field_database.setMinimumSize(QSize(0, 44))
        self.field_database.setMaximumSize(QSize(16777215, 44))
        font1 = QFont()
        font1.setFamilies(["Sans Serif"])
        font1.setPointSize(11)
        self.field_database.setFont(font1)
        self.field_database.setAutoFillBackground(False)
        self.field_database.setStyleSheet("border: 1.4px solid; border-radius:10%; background-color: palette(base); padding-top:8;padding-left:20;padding-bottom:0; padding-right:10;\n"
"border-color: rgb(154, 153, 150);\n"
"color: rgb(119, 118, 123); ")

        self.vLayoutLogin.addWidget(self.field_database)

        self.label_user = QLabel(self.gridGroupBoxMain)
        self.label_user.setObjectName("label_user")
        self.label_user.setText("Usuario")
        self.label_user.setFont(font)
        self.label_user.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_user.setAlignment(Qt.AlignBottom | Qt.AlignLeading | Qt.AlignLeft)

        self.vLayoutLogin.addWidget(self.label_user)

        self.field_user = QLineEdit(self.gridGroupBoxMain)
        self.field_user.setObjectName("field_user")
        self.field_user.setEnabled(True)
        self.field_user.setMinimumSize(QSize(0, 44))
        self.field_user.setMaximumSize(QSize(16777215, 44))
        self.field_user.setFont(font1)
        self.field_user.setAutoFillBackground(False)
        self.field_user.setStyleSheet("border: 1.4px solid; border-radius:10%; background-color: palette(base); padding-top:8;padding-left:20;padding-bottom:0; padding-right:10;\n"
"border-color: rgb(154, 153, 150);\n"
"color: rgb(119, 118, 123);")

        self.vLayoutLogin.addWidget(self.field_user)

        self.label_password = QLabel(self.gridGroupBoxMain)
        self.label_password.setObjectName("label_password")
        self.label_password.setText("Contrase\u00f1a")
        self.label_password.setFont(font)
        self.label_password.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_password.setAlignment(Qt.AlignBottom | Qt.AlignLeading | Qt.AlignLeft)

        self.vLayoutLogin.addWidget(self.label_password)

        self.field_password = QLineEdit(self.gridGroupBoxMain)
        self.field_password.setObjectName("field_password")
        self.field_password.setEchoMode(QLineEdit.Password)
        self.field_password.setMinimumSize(QSize(0, 44))
        self.field_password.setMaximumSize(QSize(16777215, 44))
        self.field_password.setFont(font1)
        self.field_password.setFocus()
        self.field_password.setAutoFillBackground(False)
        self.field_password.setStyleSheet("border: 1.4px solid; border-radius:10%; background-color: palette(base); padding-top:8;padding-left:20;padding-bottom:0; padding-right:10;\n"
"border-color: rgb(154, 153, 150);\n"
"color: rgb(119, 118, 123);")

        self.vLayoutLogin.addWidget(self.field_password)

        self.label_error = QLabel(self.gridGroupBoxMain)
        self.label_error.setObjectName("label_error")
        self.label_error.setFont(font)
        self.label_error.setStyleSheet("color: red; font: 10pt;")
        self.label_error.setAlignment(Qt.AlignBottom | Qt.AlignLeading | Qt.AlignLeft)

        self.vLayoutLogin.addWidget(self.label_error)

        self.gridLayout.addLayout(self.vLayoutLogin, 2, 0, 1, 1)
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(20)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, 10, -1, 20)

        font2 = QFont()
        font2.setBold(True)
        self.pushButtonOk = QPushButton(self.gridGroupBoxMain)
        self.pushButtonOk.setObjectName("pushButtonOk")
        self.pushButtonOk.setText("CONECTAR")
        self.pushButtonOk.setMinimumSize(QSize(0, 44))
        self.pushButtonOk.setMaximumSize(QSize(16777215, 44))
        self.pushButtonOk.setAutoDefault(True)
        self.pushButtonOk.setDefault(False)
        self.pushButtonOk.setFont(font2)
        self.pushButtonOk.setStyleSheet("border: 0.1px solid; border-radius:10%;border-color:blue;\n"
"color: rgb(255, 255, 255);\n"
"background-color: qlineargradient(spread:pad, x1:0.234, y1:0.448864, x2:0.78607, y2:0.449, stop:0 rgba(38, 163, 255, 252), stop:0.313433 rgba(52, 112, 241, 255), stop:0.666667 rgba(76, 52, 247, 255), stop:0.935323 rgba(103, 50, 246, 253));")

        self.pushButtonCancel = QPushButton(self.gridGroupBoxMain)
        self.pushButtonCancel.setObjectName("pushButtonCancel")
        self.pushButtonCancel.setText("SALIR")
        self.pushButtonCancel.setMinimumSize(QSize(0, 44))
        self.pushButtonCancel.setMaximumSize(QSize(16777215, 44))

        self.pushButtonCancel.setFont(font2)
        self.pushButtonCancel.setStyleSheet("border: 1.4px solid; border-radius:10%;  border-color: red;\n"
"color: rgb(255, 255, 255);\n"
"background-color: qlineargradient(spread:pad, x1:0.234, y1:0.448864, x2:0.78607, y2:0.449, stop:0.00995025 rgba(255, 20, 89, 252), stop:0.313433 rgba(252, 28, 86, 255), stop:0.666667 rgba(255, 58, 87, 255), stop:0.935323 rgba(246, 60, 60, 253));")

        self.verticalLayout_2.addWidget(self.pushButtonOk)
        self.verticalLayout_2.addWidget(self.pushButtonCancel)
        self.gridLayout.addLayout(self.verticalLayout_2, 9, 0, 2, 1)

        self.label_title = QLabel(self.gridGroupBoxMain)
        self.label_title.setObjectName("label_title")
        self.label_title.setText("<html><head/><body><p>Hola, Bienvenido de Nuevo</p></body></html>")
        self.label_title.setWordWrap(True)
        font3 = QFont()
        font3.setPointSize(18)
        font3.setBold(True)
        self.label_title.setFont(font3)
        self.label_title.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_title.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_title, 0, 0, 2, 1)

        self.vLayout_footer = QVBoxLayout()
        self.vLayout_footer.setSpacing(0)
        self.vLayout_footer.setObjectName("vLayout_footer")
        self.vLayout_footer.setContentsMargins(0, 0, 0, 0)
        self.label_page = QLabel(self.gridGroupBoxMain)
        self.label_page.setObjectName("label_page")
        self.label_page.setText("www.presik.com")
        font4 = QFont()
        font4.setBold(False)
        font4.setItalic(True)
        self.label_page.setFont(font4)
        self.label_page.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_page.setAlignment(Qt.AlignCenter)

        self.vLayout_footer.addWidget(self.label_page)

        self.label_version = QLabel(self.gridGroupBoxMain)
        self.label_version.setObjectName("label_version")
        self.label_version.setText(f"\u00a9 Copyright {date.today().year!s} Version {__version__}")
        self.label_version.setFont(font4)
        self.label_version.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_version.setAlignment(Qt.AlignCenter)

        self.vLayout_footer.addWidget(self.label_version)
        self.hLayout_conn = QHBoxLayout()
        self.label_icon_conn = QLabel(self.gridGroupBoxMain)
        self.label_icon_conn.setAlignment(Qt.AlignRight)
        self.label_icon_conn.setObjectName("label_icon_conn")
        self.hLayout_conn.addWidget(self.label_icon_conn)

        self.label_conn = QLabel(self.gridGroupBoxMain)
        self.label_conn.setObjectName("label_conn")
        self.label_conn.setFont(font4)
        self.label_conn.setStyleSheet("color: rgb(119, 118, 123);")
        self.label_conn.setAlignment(Qt.AlignLeft)
        self.hLayout_conn.addWidget(self.label_conn)

        self.vLayout_footer.addLayout(self.hLayout_conn)

        self.gridLayout.addLayout(self.vLayout_footer, 11, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 1, 1, 1)

        self.label_image = QLabel(self.gridGroupBoxMain)
        self.label_image.setObjectName("label_image")
        size_image = QSize(width / 12 * 6, height)
        # if screen_size == 'small':
        #    size_image = QSize(width, height)

        self.label_image.setMaximumSize(size_image)
        self.label_image.setPixmap(QPixmap(path_pos_advanced))
        self.label_image.setScaledContents(True)

        self.gridLayout_2.addWidget(self.label_image, 0, 0, 3, 1)

        # connect signals and slots
        self.pushButtonOk.clicked.connect(MainWindow.accept)
        self.pushButtonCancel.clicked.connect(MainWindow.reject)
        self.field_password.editingFinished.connect(MainWindow.accept)

        menubar = QMenuBar(self.centralwidget)
        file_menu = menubar.addMenu("Opciones")

        action_config = file_menu.addAction("Configuracion")
        action_about = file_menu.addAction("Acerca de")
        action_update = file_menu.addAction("Actualizar")

        # Connect the action to a slot or function for handling the action
        action_config.triggered.connect(MainWindow.open_config_file)
        action_about.triggered.connect(MainWindow.show_about)
        action_update.triggered.connect(MainWindow.update_system)

        self.gridLayout_2.setMenuBar(menubar)
