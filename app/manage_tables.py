import os

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QGridLayout, QPushButton

DIR_SHARE = os.path.abspath(
    os.path.normpath(os.path.join(__file__, '..', '..', 'share')))

__all__ = ['ManageTables', 'ManageZones', 'MixButton', 'MixButtonZone']

STATES = {
    'available': 'rgb(215, 215, 215)',
    'occupied': 'rgb(246, 211, 70)',
    'reserved': 'rgba(80, 190, 220, 0.8)',
}

button_style = """
    min-height: 40px;
    min-width: 100px;
"""

button_style_1 = """
    font: bold 30px;
    min-width: 100px;
    text-align: center;
"""


class MixButton(QPushButton):
    def __init__(self, value, activate, parent):
        super(MixButton, self).__init__()
        self.parent = parent
        try:
            agent = value['sale.']['agent.'].get('rec_name')
        except:
            agent = None
        self.name = value['name'] + '\n' + agent[:20] if agent else value['name']
        self.table_id = value['id']
        self.set_data(value)
        self.setText(self.name)
        self.activate = activate
        self.clicked.connect(self.activate_method)
        # self.show()

    def activate_method(self):
        self.activate(self.table_id)

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton and self.state != 'available':
            res = self.parent.action_release_table(self.table_id)
            if res:
                self.set_data(res)
        super(MixButton, self).mousePressEvent(event)

    def set_data(self, record):
        self.state = record['state']
        self.sale_id = record['sale']
        color = STATES[self.state]
        new_style_ = f"""
            min-height: 90px;
            min-width: 100px;
            background-color: {color};
            font-weight: bold;
            border-width: 0px;
            border-radius: 15px;
        """
        self.setStyleSheet(new_style_)


class ManageTables(QGridLayout):

    def __init__(self, parent, tables):
        super(ManageTables, self).__init__()
        self.setHorizontalSpacing(1)
        self.setVerticalSpacing(1)
        self.parent = parent
        self.columns = 6
        self.buttons = {}

    def update_records(self, records):
        while self.count():
            item = self.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.hide()
        self.buttons = {}

        rows = int(len(records) / self.columns) + 1
        positions = [(i, j) for i in range(rows) for j in range(self.columns)]
        for position, value in zip(positions, records, strict=False):
            button = MixButton(value, self.activate_method, self.parent)
            self.buttons[button.table_id] = button
            self.addWidget(button, *position)

    def activate_method(self, table_id):
        button = self.buttons[table_id]
        if self.parent.sale_id and button.state == 'available':
            res = self.parent.action_assign_table(table_id, button.name)
            if res['to_add']:
                button.set_data(res['to_add'])
            if res['to_drop']:
                button = self.buttons[res['to_drop']['id']]
                button.set_data(res['to_drop'])
        elif button.state == 'occupied':
            self.parent.action_see_table(table_id)


class ManageZones(QGridLayout):

    def __init__(self, parent, zones):
        super(ManageZones, self).__init__()
        self.setHorizontalSpacing(1)
        self.setVerticalSpacing(1)
        self.parent = parent
        columns = 6
        rows = int(len(zones) / columns) + 1
        self.buttons = {}
        positions = [(i, j) for i in range(rows) for j in range(columns)]
        for position, value in zip(positions, zones, strict=False):
            button = MixButtonZone(value, self.activate_method, self.parent)
            self.buttons[button.table_id] = button
            self.addWidget(button, *position)

    def update_records(self, records):
        for rec in records:
            button = self.buttons[rec['id']]
            button.set_data(rec)

    def activate_method(self, zone_id):
        button = self.buttons[zone_id]
        self.parent.action_open_tables(zone_id, button.name)


class MixButtonZone(QPushButton):
    def __init__(self, value, activate, parent):
        super(MixButtonZone, self).__init__()
        self.parent = parent
        self.name = value['name']
        self.table_id = value['id']
        self.tables = value['tables']
        self.set_data(value)
        self.setText(self.name)
        self.activate = activate
        self.clicked.connect(self.activate_method)
        self.show()

    def activate_method(self):
        self.activate(self.table_id)

    def set_data(self, record):
        new_style_ = """
            min-height: 90px;
            min-width: 100px;
            background-color: rgb(215, 215, 215);
            font-weight: bold;
            border-width: 0px;
            border-radius: 15px;
        """
        self.setStyleSheet(new_style_)
