import os
from datetime import date

def printf(_sale):
    print("*" * 100)
    print(_sale)


def read_files_scale():
    try:
        if os.name == 'posix':
            dir = '/home/psk/Documents/DIBAL'
        else:
            dir = r'D:\USUARIOS\USUARIO\Desktop\DIBAL'
        files = os.listdir(dir)
    except:
        dir = None
        files = []

    today = str(date.today())
    new_sales = []
    backup_dir = os.path.join(dir, today)
    if not os.path.exists(backup_dir):
        os.mkdir(backup_dir)
    for _file in files:
        if not '.txt' in _file:
            continue

        pathfile = os.path.join(dir, _file)
        pathfile_bck = os.path.join(backup_dir, _file)
        new_sale = None
        with open(pathfile) as f:
            new_sale = {}
            lines = f.readlines()
            lines_to_add = []
            for line in lines:
                product = line[:5].rstrip()
                weight = line[5:11].rstrip()
                unit_price = line[11:16].rstrip()
                amount = line[16:22].rstrip()
                date_ = line[22:28].rstrip()
                time_ = line[28:32].rstrip()
                salesman = line[32:37].rstrip()
                scale_code = line[37:39].rstrip()
                ticket = line[39:].rstrip()
                if not new_sale:
                    new_sale = {
                        'date_': date_,
                        'time_': time_,
                        'salesman': salesman[1:],
                        'scale_code': scale_code,
                        'ticket': ticket,
                        'lines': [],
                    }
                line = {
                    'product': product,
                    'weight': weight,
                    'unit_price': unit_price,
                    'amount': amount,
                    'date_': date_,
                    'time_': time_,
                }
                lines_to_add.append(line)
            new_sale['lines'] = lines_to_add
            new_sales.append(new_sale)
            printf(new_sale)
        os.rename(pathfile, pathfile_bck)
    return new_sales



# with open(fullpath) as f:
#     lines = f.readlines()
#
#
# for line in lines:
#     print("*" * 100)
#     print("leyendo el archivo.....", name_file)
#     print(line)
#
#     code_product = line[:5]
#     weight = line[5:11]
#     unit_price = line[11:16]
#     amount = line[16:22]
#     date_ = line[22:28]
#     time_ = line[28:32]
#     salesman = line[32:37]
#     scale_code = line[37:39]
#     ticket = line[39:]
#     # code_product
#     print('code_product ', code_product)
#     print('weight ', weight)
#     print('unit_price ', unit_price)
#     print('amount ', amount)
#     print('date_ ', date_)
#     print('time_ ', time_)
#     print('salesman ', salesman)
#     print('scale_code ', scale_code)
#     print('ticket ', ticket)
